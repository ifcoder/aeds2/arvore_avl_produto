# Estrutura de Dados | Arvore AVL (Produto)

- Este código C++ pertence a disciplina Algoritmos e Estrutura de Dados II
- Peço atenção pois antes de implementar a arvore AVL é necessário entender bem os conceitos. 
- [Material de estudo](https://summer-pocket-6a4.notion.site/3-5-Codificando-rvore-AVL-0e8a0e64a36743d5b5bd1d60de3057f8) 

  
## Linguagem C++
- O estudo desta disciplina é feito usando a linguagem de programação C++

## NetBeans
- Este projeto foi desenvolvido utilizando a IDE Netbeans. 
- Para rodá-lo é necessário ter o compilador C++ em sua máquina e a IDE NetBeans configurada para a linguagem C++
